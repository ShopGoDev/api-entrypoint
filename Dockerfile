# BASE PHP-FPM ------------------------------------------------------------------------------------------
# This Base Php-FPM docker image can be seperated into other repository
# -------------------------------------------------------------------------------------------------------
FROM quay.io/cashrewards/php-base-ms

# SOURCE CODE COPY --------------------------------------------------------------------------------------
COPY ./src/ /var/www/html/

# COMPOSER ----------------------------------------------------------------------------------------------
ENV COMPOSER_ALLOW_SUPERUSER 1
RUN /usr/local/bin/composer self-update \
    && cd /var/www/html \
    && /usr/local/bin/composer install

# SET FILE PERMISSION -----------------------------------------------------------------------------------
RUN chown -R :www-data /var/www/html \
 && chmod -R ug+rwx /var/www/html/storage /var/www/html/bootstrap/cache

# THESE VOLUMES are shared with nginx container using "volumes_from"
VOLUME ["/etc/nginx/conf.d/", "/var/www/html/"]